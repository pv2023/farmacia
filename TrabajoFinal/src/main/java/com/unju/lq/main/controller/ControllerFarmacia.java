package com.unju.lq.main.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ControllerFarmacia {

	@GetMapping("/")
	public String index() {
		return "index";
	}
	
	@GetMapping("/medicamentos")
	public String medicamentos() {
		return "medicamentos";
	}
	
	@GetMapping("/usuarios")
	public String usuarios() {
		return "usuarios";
	}
}
